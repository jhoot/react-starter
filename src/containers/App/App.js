import React, { Component } from 'react';
import styles from './App.scss';

class App extends Component {
  render() {
    let clas = (styles.app + " hello");
    return (
      <div className={clas}>
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
