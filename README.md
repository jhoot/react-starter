# React Starter Kit

I created this project to have a good starting application, modularized, ejected and set up with CSS module imports and a SCSS preprocessor.

### Instructions

Clone this file to have a brand new, empty ReactJS project already configured and working with style imports and SCSS. Change the name to suit your needs, or build away!